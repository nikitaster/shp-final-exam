import os

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from main.models import Snippet


class TestIndexPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()
        user = User.objects.get(username='vasya')
        self.c.force_login(user)
        self.response = self.c.get(reverse('index'))

    def test_simple(self):
        self.assertEqual(self.response.context['pagename'], 'PythonBin v2.0')
        self.assertEqual(self.response.context['user'].username, 'vasya')
        self.assertTemplateUsed(self.response, 'pages/index.html')


class TestAddSnippetPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()
        user = User.objects.get(username='vasya')
        self.c.force_login(user)
        self.response = self.c.get(reverse('add_snippet'))

    def test_simple_get(self):
        self.assertEqual(self.response.context['pagename'], 'Добавление нового сниппета')
        self.assertTemplateUsed(self.response, 'pages/add_snippet.html')

    def test_simple_post(self):
        response = self.c.post(reverse('add_snippet'), {
            'name': '123', 'code': 'import this'
        })
        record = Snippet.objects.filter(name='123')
        self.assertEqual(len(record), 1)


class TestPep8SnippetPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()
        user = User.objects.get(username='vasya')
        self.c.force_login(user)
        self.c.post(reverse('add_snippet'), {'name': '123', 'code': '   a    =   b   +   c   '})
        self.record = Snippet.objects.filter(name='123').last()

    def test_get(self):
        response = self.c.get(reverse('view_format', kwargs={'snippet_id': self.record.id, 'utility': 'pep8'}))
        self.c.post(reverse('add_snippet'), {'name': '123', 'code': '   a    =   b   +   c   '})
        self.record = Snippet.objects.filter(name='123').last()
        self.assertEqual(response.context['code'], 'a = b + c\n')
        self.assertTrue(os.path.exists(self.record.get_filename('autopep8')))

    def test_invalid_get(self):
        response = self.c.get(reverse('view_format', kwargs={'snippet_id': 100, 'utility': 'pep8'}))
        self.assertEqual(response.status_code, 404)


class TestMySnippetPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()
        user = User.objects.get(username='vasya')
        self.c.force_login(user)
        self.response = self.c.get(reverse('my_snippets'))

    def test_get(self):
        self.assertEqual(self.response.status_code, 200)


class TestUnifySnippetPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()
        user = User.objects.get(username='vasya')
        self.c.force_login(user)
        self.c.post(reverse('add_snippet'), {'name': '123', 'code': 'print("hello")'})
        self.record = Snippet.objects.filter(name='123').last()

    def test_get(self):
        response = self.c.get(reverse('view_format', kwargs={'snippet_id': self.record.id, 'utility': 'unify'}))
        self.c.post(reverse('add_snippet'), {'name': '123', 'code': 'print("hello")'})
        self.record = Snippet.objects.filter(name='123').last()
        self.assertEqual(response.context['code'], "print('hello')")
        self.assertTrue(os.path.exists(self.record.get_filename('unify')))

    def test_invalid_get(self):
        response = self.c.get(reverse('view_format', kwargs={'snippet_id': 100, 'utility': 'unify'}))
        self.assertEqual(response.status_code, 404)


class TestDeleteSnippetPageSnippetPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()
        user = User.objects.get(username='vasya')
        self.c.force_login(user)
        self.c.post(reverse('add_snippet'), {'name': '123', 'code': '   a    =   b   +   c   '})
        self.record = Snippet.objects.filter(name='123').last()

    def test_get(self):
        response = self.c.get(reverse('delete_snippet', kwargs={'snippet_id': self.record.id}))
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.c.post(reverse('delete_snippet', kwargs={'snippet_id': self.record.id}))
        self.assertEqual(response.status_code, 200)


class TestLoginPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()

    def test_post_fail(self):
        response = self.c.post(reverse('login'))
        self.assertEqual(response.status_code, 302)

    def test_post_access(self):
        response = self.c.post(reverse('login'), data={'username': 'UnitTest', 'password': 'Pa$$w0rd'}, follow=True)
        self.assertEqual(response.status_code, 200)


class TestViewSnippetPage(TestCase):
    fixtures = ['test_db.json']

    def setUp(self):
        self.c = Client()
        user = User.objects.get(username='vasya')
        self.c.force_login(user)
        self.c.post(reverse('add_snippet'), {'name': '123', 'code': '   a    =   b   +   c   '})
        self.record = Snippet.objects.filter(name='123').last()

    def test_get_method(self):
        response = self.c.get(reverse('view_snippet', kwargs={'snippet_id': self.record.id}))
        self.assertEqual(response.status_code, 200)